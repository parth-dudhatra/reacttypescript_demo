import { log } from 'console';
import React from 'react'

// Lexical this
// --> here two ways to decalre to this 1- calling context and 2- lexical scope

    class Person {
      private _age: number;
      constructor(_age : number){
        this._age = _age;
      }
      growold = () => {
        this._age++;
      } 
      age() {
        return this._age
      }
    }
    
    const person = new Person(1);
    const growold = person.growold();
    setTimeout(person.growold, 1000)
    // growold();
    console.log("age: ", person.age());

// readOnly

class Animal {
  readonly name: string;
  constructor(name: string) {
    this.name = name
  }
}

const sheep = new Animal("sheep");  // Allow
console.log(sheep.name) //Allow
// sheep.name = "dog" 
// sheep.name = "wolf"


// Union Types

/**
 * Takes a string and adds `padding` to the left.
 * If `padding` is a number then that number of spaces is added to the left.
 * If `padding` is a string then `padding` is appended to the left.
 * @returns 
 */
type Pedding = string | number
function padLeft(value: string, padding: Pedding) {
  if( typeof padding === "number"){
    return Array(padding +1).join(' ') + value;
  }
  if( typeof padding === "string"){
    return padding + value;
  }
  throw new Error(`Expected number or string, got '${padding}'` )
}

padLeft("Hello world", 4);
padLeft("Hello world", ' ');
padLeft("Hello world", '---');
// padLeft("Hello world", false);


// Type Narrowing

class Cat { 
  meow() {
    console.log("meow")
  }
}
class Dog {
  bark() {
    console.log('bark');
  }
}

type Animals = Cat | Dog;

function speak(animal : Animals){
  if( animal instanceof Cat) {
    animal.meow()
  }
  if( animal instanceof Dog){
    animal.bark();
  }
}
speak(new Dog)

// another exmples
type Squre = {
  size: number
}

type Rectangle ={
  width: number,
  height: number
}

type Shape = Squre | Rectangle

function area(shape: Shape){
  if('size' in shape){
    return shape.size * shape.size;
  }
  if('width' in shape){
    return shape.width * shape.height
  }
}

area({ size: 2});
area({ width: 2, height: 3});

// class parameter properties

class Persons {
  constructor(
    public name: string,
    public age: number
  ){
  }
}

const adam = new Persons("adam", 120000);
console.log(adam.name, adam.age); // "adam", 120000




// null versus undefind
let notDefined: undefined = undefined;
let notPersent: null = null;

class Point {
  x!: number;
  y!: number;
}

const center = new Point();
center.x = 0;
console.log(center.x , center.y);

function logVowels(value: string) {
   console.log(value.match(/[aeiou]/gi))
}

logVowels('hello'); // [e,o]
logVowels('tdr') // null

console.log(null == undefined) // true

function decorde( value: string | null | undefined) {
  // if value show compire time error then write a blow of the code
  if(value == null){
    return value
  }
  return `--${value.trim()}--`;
}
console.log(decorde("hello")) // --hello--


// Non-null Assertion operator
type Person1 = {
  name: string;
  email?: string | undefined | null;
}

function sendEmial(email: string) {
  console.log("Send Emial to", email);
}

function chekEmiakType(person : Person1) {
  if(person.email == null) throw new Error(`Person ${person.name} is not contctable`)
}

function contact(person: Person1) {
  sendEmial(person.email!);
}

// intefaces
// here 2 exmple to the understand the interface 
// 1 exmple is without using interfces

type Point4D = {
  x: number,
  y: number
}

type Point5D = Point4D & {
  z: number
}

export const point: Point5D = {
  x: 3,
  y: 5,
  z: 6
}

// and now show exmples with interfaces
interface PointR {
  x: number,
  y: number
}
interface PointB extends PointR {
  z: number
}
export const pokl: PointB = {
  x: 3,
  y: 5,
  z: 5,
}

// type verses interfeces
// interfeces

export interface InputProps {
  type : 'text' | 'email',
  value: string,
  onChange: (newValue: string) => void; 
}

// type

type InputOnChange = (newValue: string) => void;
type InputValue = string;
type InputType = 'text' | 'email'

export type Roll = {
  type: InputType,
  value: InputValue,
  onChange: InputOnChange
}

function Exmples() {
  return (
    <div>Exmples</div>
  )
}

export default Exmples