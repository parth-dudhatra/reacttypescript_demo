import React from 'react';
import { isPalindrome } from './modules/utiltes';

//Primitive type
let isPreset: boolean = false;
let magic: number =  66.6;
let hellos: string = "hello world"

let notDefined: undefined = undefined;
let notPresent: null = null;

let penta: symbol = Symbol('star');
// let biggy: bigint = 24n;



// Instance Types
let regexp: RegExp = new RegExp('ab+c');
let array:Array<number> = [1,2,5];
let set:Set<number> = new Set([1,3,4])


// array and tuples
let arrayes: number[] = [1, 3, 4]
arrayes = [1];
// arrayes = ["part"] // Error

let tuples: [number, number] = [0, 0];
// tuples = [1] // false
tuples= [1, 1] // true
// tuples= ["string", 2] //false



// object types and type Aliases
        // exmples of object type
        let center: { x: number , y: number} = {
            x: 0,
            y: 0,
        }

        // exmples of Aliases 
        type point = { x: number , y: number}
        let cent: point ={ x: 0, y: 0}


// functions
        type Add = (a : number, b: number) => number;
        let add :Add;
        add = (a, b) => a+b;
        console.log(add(3,6));

//Structural Typing 
type Point2D = { x: number, y: number};
type Point3D = { x: number, y: number, z:number};

let point2D: Point2D = { x: 1, y: 5};
let point3D: Point3D = { x: 3, y: 5, z: 4}

 // extre info okay
 point2D = point3D
 function takesPont2D(point : Point2D) {}
 takesPont2D(point3D);
 // Error: missing info
//  point3D = point2D;
//  function takesPont3D(point : Point3D) {}
//  takesPont3D(point2D);


// Classes
        class Animal {
             name : string  // this variable also declartion to pravite, public and protected 

            constructor(name : string) {
                this.name = name
            }

            move(distanceInMeters: number){
                console.log(`${this.name} moved ${distanceInMeters}m`)
            }
        }

        let cat = new Animal("Cat");
        cat.name = "dog"
        cat.move(10);


// generics

class Queue<B> {
     data:B[] = [];
    push(item:B){this.data.push(item)}
    pop() { return this.data.shift()}
}

const queue = new Queue<number>();
queue.push(123);
queue.push(435);

// console.log(queue.pop().toPrecision(1));

// special types: any and unknown
    let exmAny: any;
    let exmUnknown : unknown;

    //Any
    exmAny = 123;
    exmAny = "hello";

    //unknown
    exmUnknown = 123;
    exmUnknown = "wod"

    // any 
    // exmAny.allows.anything.you.can.imagine();
    let anySetBool: Boolean = exmAny

    // unknown
    if( typeof exmUnknown == "string") {
        exmUnknown.trim();
    }
    if( typeof exmUnknown == "boolean"){
        let unknownSetBool: boolean = exmUnknown
    }

 
//Type Assertions
const load = () => {
    return "parth"
}
let hello = load();
const trimmed = (hello as string).trim();

if( typeof hello === "string") {
    const paj = hello.trim();
}

//Type Casting 
let lees;
lees = "234"

 // use as number
  const number = +lees;

console.log(number == 234); // true
console.log(number) // 234;



// create a module and how to import them and use the function
console.log(isPalindrome('madam'));



// type declarations
console.log(
    'Loggged User',
    process.env.USER
)


//async await
const delay = (ms : number) => new Promise(res => setTimeout(res, ms));

const mainAsync = async() => {
    await delay(1000);
    console.log("1s");
    await delay(1000);
    console.log("2s");
    await delay(1000);
    console.log("3s");
}
mainAsync();



function Parth() {
  return (
    <div>Parth</div>
  )
}

export default Parth